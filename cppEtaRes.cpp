#include <TROOT.h>
#include <TFile.h>
#include <TEfficiency.h>
#include <string>

string REFERENCEOBJECT = "tau";
string INPUTFILE = "/afs/cern.ch/user/j/jmontejo/public/ForAlex/rpv1l_L1eta/ttbar_JETM6/data-ntuple/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_JETM6.e6337_s3126_r9364_p3916.root";
//string INPUTFILE = "/afs/cern.ch/user/j/jmontejo/public/ForAlex/rpv1l_newL1eta/ttbar_JETM6/data-ntuple/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_JETM6.e6337_s3126_r9364_p3916.root";
int debug=0;
float LOWER_REFERENCE_ETA_CUT = 2.3;
float UPPER_REFERENCE_ETA_CUT = 2.5;
float REFERENCE_ISOLATION_CUT = 0.6;
float dPHI = 0.3;
float dETA = 0.6;

//TH1F *h_total_jets = new TH1F( "h_total_jets", "h_total_jets", 2, 0, 2 );
//TH1F *h_total_matches = new TH1F( "h_total_matches", "h_total_matches", 2, 0, 2 );
//TH1F *h_no_match = new TH1F( "h_no_match", "h_no_match", 2, 0, 2 );
//TH1F *h_duplicate = new TH1F( "h_duplicate", "h_duplicate", 2, 0, 2 );
int nIsolatedJets = 0;
int nMatches = 0;
int nNoMatches = 0;
int nDuplicateMatches = 0;
int nNonDuplicateMatches = 0;

/*
overlay
    total, positive, negative jets
    total jets electron tau
*errors
for jets:
    for offline jets above 60000 whats the fraction without l1 match.
    What fraction has double match
post on jira
*/

std::vector< tuple<float,float> > getMatchedL1Eta(  ROOT::VecOps::RVec<float>& reference_pt,
                                            ROOT::VecOps::RVec<float>& reference_eta,
                                            ROOT::VecOps::RVec<float>& reference_phi,
                                            ROOT::VecOps::RVec<float>& reference_e,
                                            ROOT::VecOps::RVec<float>& probe_pt,
                                            ROOT::VecOps::RVec<float>& probe_eta,
                                            ROOT::VecOps::RVec<float>& probe_phi,
                                            ROOT::VecOps::RVec<float>& probe_e)
{

    if(debug) cout<<"------------------------- NEW EVENT-------------------------"<<endl;
    //Get reference TLorentzvectors
    std::vector< TLorentzVector > reference_original_TLVs;
    TLorentzVector reference_TLV;
    if(debug) cout<<"Offline: "<<endl;
    for(int i=0;i<reference_pt.size();i++){
        //if(reference_eta.at(i)>0) continue;
        //if(reference_pt.at(i)<60000) continue;
        if( abs(reference_eta.at(i))<LOWER_REFERENCE_ETA_CUT || UPPER_REFERENCE_ETA_CUT<abs(reference_eta.at(i)) ) continue;
        reference_TLV.SetPtEtaPhiE(reference_pt.at(i),reference_eta.at(i),reference_phi.at(i),reference_e.at(i));
        reference_original_TLVs.push_back(reference_TLV);
        if(debug) cout<<setprecision(2)<<i<<":"<<"\t"<<reference_pt.at(i)<<"\t\t"<<reference_eta.at(i)<<"\t\t"<<reference_phi.at(i)<<"\t\t"<<reference_e.at(i)<<endl;

    }
    
    //Get probe TLorentzvectors
    std::vector< TLorentzVector > probe_original_TLVs;
    TLorentzVector probe_TLV;
    if(debug) cout<<endl<<"Online: "<<endl;
    for(int i=0;i<probe_pt.size();i++){
        probe_TLV.SetPtEtaPhiE(probe_pt.at(i),probe_eta.at(i),probe_phi.at(i),probe_e.at(i));
        probe_original_TLVs.push_back(probe_TLV);
        if(debug) cout<<setprecision(2)<<i<<":"<<"\t"<<probe_pt.at(i)<<"\t\t"<<probe_eta.at(i)<<"\t\t"<<probe_phi.at(i)<<"\t\t"<<probe_e.at(i)<<endl;
    }
    
    //Cut isolated reference TLVS
    if(debug) cout<<endl<<"ISOLATION"<<endl;
    std::vector< TLorentzVector > isolated_reference_TLVs;
    float dR = 999;
    for(int i=0;i<reference_original_TLVs.size();i++){
        for(int j=0;j<reference_original_TLVs.size();j++){
            dR = 999;
            if(i==j) continue;
            dR = reference_original_TLVs.at(i).DeltaR(reference_original_TLVs.at(j));
            if(dR<REFERENCE_ISOLATION_CUT){
                if(debug) cout<<"Offline jet "<<j<<" and ";
                break;
            }
        }
        if(dR<REFERENCE_ISOLATION_CUT){
            if(debug) cout<<i<<" have a dR of "<< dR<<endl;
            continue;
        }
        isolated_reference_TLVs.push_back(reference_original_TLVs.at(i));

    }
    
    nIsolatedJets += isolated_reference_TLVs.size();
    
    if(debug){cout<<endl<<"Isolated Offline: "<<endl; for(int i=0;i<isolated_reference_TLVs.size();i++){cout<<setprecision(2)<<i<<":"<<"\t"<<isolated_reference_TLVs.at(i).Pt()<<"\t\t"<<isolated_reference_TLVs.at(i).Eta()<<"\t\t"<<isolated_reference_TLVs.at(i).Phi()<<"\t\t"<<isolated_reference_TLVs.at(i).E()<<endl;} }
    if(debug){cout<<endl<<"Online: "<<endl; for(int i=0;i<probe_original_TLVs.size();i++){cout<<setprecision(2)<<i<<":"<<"\t"<<probe_original_TLVs.at(i).Pt()<<"\t\t"<<probe_original_TLVs.at(i).Eta()<<"\t\t"<<probe_original_TLVs.at(i).Phi()<<"\t\t"<<probe_original_TLVs.at(i).E()<<endl;} }
    
    
    //Match reference to probe
    if(debug) cout<<endl<<"MATCHING"<<endl;
    std::vector< tuple<int,int> > indexPair_vector;
    int didMatch=0;
    for(int i=0;i<isolated_reference_TLVs.size();i++){
        didMatch=0;
        for(int j=0;j<probe_original_TLVs.size();j++){
            if(abs(probe_original_TLVs.at(j).Phi() - isolated_reference_TLVs.at(i).Phi()) > dPHI) continue;
            if(debug) cout<<"Offline jet "<<i<<" and L1 jet "<<j<<" have dPhi<cut"<<endl;
            
            if(abs(probe_original_TLVs.at(j).Eta() - isolated_reference_TLVs.at(i).Eta()) > dETA) continue;
            if(debug) cout<<"Offline jet "<<i<<" and L1 jet "<<j<<" have dEta<cut"<<endl;
            
            if(debug) cout<<"MATCH: "<<i<<":"<<j<<endl;
            didMatch=1;
            indexPair_vector.emplace_back(i,j);
            nMatches += 1;
        }
        if(didMatch==0) nNoMatches += 1;
    }
    
    
    if(debug){cout<<endl<<"All Matches"<<endl; for(int i=0;i<indexPair_vector.size();i++){cout<<get<0>(indexPair_vector.at(i))<<":"<<get<1>(indexPair_vector.at(i))<<endl;}}
    
    //Find non-duplicate matches and fill return vector
    if(debug) cout<<endl<<"Filling non-duplicates"<<endl;
    bool isDuplicate=false;
    std::vector< tuple<float,float> > etaVector;
    
    for(int i=0;i<indexPair_vector.size();i++){
        for(int j=0;j<indexPair_vector.size();j++){
            if(i==j) continue;
            isDuplicate = (get<0>(indexPair_vector.at(i))==get<0>(indexPair_vector.at(j)) || get<1>(indexPair_vector.at(i))==get<1>(indexPair_vector.at(j)));
            if(isDuplicate) break;
        }
        if(isDuplicate){
            if(debug) cout<<"Duplicate: "<<get<0>(indexPair_vector.at(i))<<":"<<get<1>(indexPair_vector.at(i))<<endl;
            nDuplicateMatches += 1;
            continue;
        }
        if(debug) cout<<"Filling: "<<get<0>(indexPair_vector.at(i))<<":"<<get<1>(indexPair_vector.at(i))<<endl;
        nNonDuplicateMatches +=1;
        etaVector.emplace_back((abs(isolated_reference_TLVs.at(get<0>(indexPair_vector.at(i))).Eta())) , abs(probe_original_TLVs.at(get<1>(indexPair_vector.at(i))).Eta()));
    }
    
    return etaVector;
}

int jetByJet(){

    string reference_pt  = REFERENCEOBJECT+"_pt";
    string reference_eta = REFERENCEOBJECT+"_eta";
    string reference_phi = REFERENCEOBJECT+"_phi";
    string reference_e   = REFERENCEOBJECT+"_e";
    
    string probe_pt = "L1jet_pt";
    string probe_eta = "L1jet_eta";
    string probe_phi = "L1jet_phi";
    string probe_e = "L1jet_e";

    //Loading RDataFrame
    cout<<"Loading RDataFrame....";
    ROOT::RDataFrame df("CollectionTree", INPUTFILE);
    cout<<"\t\t\tdone."<<endl;

    //Processing RDataFrame
    cout<<"Processing RDataFrame....";
    auto df_passed_reference = df
                                //.Range(0,100)
                                //.Range(79,80)//Demonstrating duplicate match cut
                                //.Range(44,45)//Demonstrating Isolation cut
                                //.Range(360556,360560)
                                //.Range(0,1000000)
                                .Filter("n_L1jet>0")
                                .Filter("Min(L1jet_pt)>15000")
                                .Define("etas",getMatchedL1Eta,{reference_pt, reference_eta, reference_phi, reference_e, probe_pt, probe_eta, probe_phi, probe_e});
    cout<<"\t\tdone."<<endl;
    
    TH1F *h_total_passed = new TH1F( "h_total_passed", "h_total_passed", 80, 0.025, 4.025 );
    TH1F *h_reference_eta = new TH1F( "h_reference_eta", "h_reference_eta", 80, 0.025, 4.025);
    TH1F *h_probe_eta = new TH1F( "h_probe_eta", "h_probe_eta", 80, 0.025, 4.025 );
    TEfficiency* myEff = new TEfficiency("eff","my efficiency",80,0.025, 4.025 );
    
    //Getting list of int
    cout<<"Filling Histograms.... "<<endl;
    auto entryList = df_passed_reference.Take<std::vector< tuple<float,float> >>("etas");
    for (auto entry : *entryList){
        for(int i=0;i<entry.size();i++){
            //cout<<get<1>(entry.at(i))<<endl;
            h_reference_eta->Fill(get<0>(entry.at(i)));
            h_probe_eta->Fill(get<1>(entry.at(i)));
            for(int j=0;j<h_probe_eta->GetNbinsX()+2;j++){
                myEff->Fill(h_probe_eta->GetBinCenter(j) >= get<0>(entry.at(i)), h_probe_eta->GetBinCenter(j));
            }
        }
    }
    
    //cout<<"Trying to print one int...\t\t";
    //cout<<entryList->at(0)<<endl;//FIXME: This is absolutely necessary for some reason
    
    //Filling total historgram
    cout<<endl<<"Filling total historgram....";
    int numberOfPassedReference = h_probe_eta->GetEntries();
    for(int i=0;i<h_total_passed->GetNbinsX()+2;i++){//0 and +2 cover underflow and overflow bins
        h_total_passed->SetBinContent(i,numberOfPassedReference);
        h_total_passed->SetBinError(i,sqrt(numberOfPassedReference));
    }
    cout<<"\tdone."<<endl;

    //Making Efficiency object
    cout<<"Making Efficiency object....";
    TEfficiency *pEff = new TEfficiency(*h_probe_eta->GetCumulative(),*h_total_passed);
    cout<<"\t\tdone."<<endl;


    //Draw objects
    TCanvas *c1 = new TCanvas("c1","c1",800,800);
    c1->Divide(2,2);
    c1->cd(1);
    h_reference_eta->Draw("E0");
    c1->cd(2);
    h_probe_eta->Draw("E0");
    c1->cd(3);
    h_probe_eta->GetCumulative()->Draw("E0");
    c1->cd(4);
    pEff->Draw();
    TGraphAsymmErrors * graph = pEff->CreateGraph();
    TGraphAsymmErrors * coarsegraph = new TGraphAsymmErrors(6);
    coarsegraph->SetName("graph");
    Double_t x, y;
    for(int p=0;p<graph->GetN();p++){
        graph->GetPoint(p,x,y);
        if(fabs(x-2.0)<1e-4){
            coarsegraph->SetPoint(0,x,y); 
            coarsegraph->SetPointError(0,0.1,0.1,graph->GetErrorYlow(p),graph->GetErrorYhigh(p));
        }
        if(fabs(x-2.2)<1e-4){
            coarsegraph->SetPoint(1,x,y); 
            coarsegraph->SetPointError(1,0.1,0.1,graph->GetErrorYlow(p),graph->GetErrorYhigh(p));
        }
        if(fabs(x-2.45)<1e-4){
            coarsegraph->SetPoint(2,x,y); 
            coarsegraph->SetPointError(2,0.15,0.1,graph->GetErrorYlow(p),graph->GetErrorYhigh(p));
        }
        if(fabs(x-2.65)<1e-4){
            coarsegraph->SetPoint(3,x,y); 
            coarsegraph->SetPointError(3,0.1,0.15,graph->GetErrorYlow(p),graph->GetErrorYhigh(p));
        }
        if(fabs(x-2.95)<1e-4){
            coarsegraph->SetPoint(4,x,y); 
            coarsegraph->SetPointError(4,0.15,0.15,graph->GetErrorYlow(p),graph->GetErrorYhigh(p));
        }
        if(fabs(x-3.9)<1e-4){
            coarsegraph->SetPoint(5,x,y); 
            coarsegraph->SetPointError(5,0.8,0.1,graph->GetErrorYlow(p),graph->GetErrorYhigh(p));
        }
    }


    //Save objects
    TFile *outFile = new TFile(TString(REFERENCEOBJECT+".root"),"RECREATE");
    outFile->cd();
    h_probe_eta->Write();
    h_reference_eta->Write();
    h_total_passed->Write();
    pEff->Write();
    myEff->Write();
    coarsegraph->Write();
    outFile->Close();
    
    cout<<endl;
    cout<<"Number of isolated offline jets: "<<nIsolatedJets<<endl;
    cout<<"Number of no-match offline jets: "<<nNoMatches<<endl;
    cout<<"Fraction of no-match offline jets: "<<nNoMatches/(double)nIsolatedJets<<endl;
    cout<<endl;
    cout<<"Number of total matches: "<<nMatches<<endl;
    cout<<"Number of duplicate matches: "<<nDuplicateMatches<<endl;
    cout<<"Fraction of duplicate matches: "<<nDuplicateMatches/(double)nMatches<<endl;
    cout<<endl;
    cout<<"Number of non-duplicate matches: "<<nNonDuplicateMatches<<endl;
    cout<<"duplicate + non-duplicate: "<<nDuplicateMatches+nNonDuplicateMatches<<endl;
    
    return 0;
}

void cppEtaRes(){
    jetByJet();

}
