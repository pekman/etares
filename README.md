# Setup
1. git clone https://gitlab.cern.ch/pekman/etares.git
2. In "cppEtaRes.cpp", the input file is defined by "INPUTFILE" at the top of the file. This is know to work for DAODs.
3. In "cppEtaRes.cpp", the desired reference object (offline object such as jet, electron, or tauon) is defined by "REFERENCEOBJECT"
  - The default output name is "REFERENCEOBJECT.root"
4. Set the other global variables at the top of the file such as phi, eta, pT, and isolation cuts as desired

# Running
1. root -l cppEtaRes.cpp

# Plotting
The plotting currentyl only plots with three input TGraphs, but this is easily changed
1. Define the input files created by "cppEtaRes.cpp", at the top of "newPlotting.py"
2.




>>>Change "ref" variable in cppETaREs.cpp to "jet"
root -l cppEtaRes.cpp
>>>Change "ref" variable in cppETaREs.cpp to "el"
root -l cppEtaRes.cpp
>>>Change "ref" variable in cppETaREs.cpp to "tau"
root -l cppEtaRes.cpp
python plotting/newPlotting.py ./all.pdf
