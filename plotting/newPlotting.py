from ROOT import gROOT, TH1, TH1D, THStack, TLine, TF1
import ROOT
import sys

from TH1_helpers import TH1_helper
from layout_helpers import layout_helper
from plotting_helpers import simplePlot
from plotting_helpers import ratioPlot

def exampleSimplePlot(fileOut):

    inFile = ROOT.TFile.Open("./jet.root")
    gr1=inFile.Get("graph")
    inFile = ROOT.TFile.Open("./el.root")
    gr2=inFile.Get("graph")
    inFile = ROOT.TFile.Open("./tau.root")
    gr3=inFile.Get("graph")

    #print(gr1.GetN())

    #return

    ph = simplePlot()
    ph.setStyle(24)
    lh = layout_helper()
    # Prepare canvas and pads
    pads = ph.canvasAndPads(False)
    # Calculate text objects coordinates
    #    and draw all objects.
    plots = [gr1,gr2,gr3]

    #Used for verifying code with old versions    
    for gr in plots:
        print("new graph:")
        for i in range(0,gr.GetN()):
            print(gr.GetX()[i],gr.GetY()[i])


    lh.resetAttributes(pad = pads[0], plots = plots
                       , xMin = 1.7, xMax = 3.0
                       , yMin = 0.0 , yMax = 1.3
                       , legendNCol = 1
                       , letAtlasLabelFloat = False, yMaxOffset = 0.05, yMinOffset = 0.05
                       , textPlotsDist = 0.04999
                       , considerUncertainties = True
                       , topTextPosY = ph.topTextPosY, bottomTextPosY = ph.bottomTextPosY
                       , verbose = True, entryHeight = 0.065
                       , putTextAtTheTop = True, putTextAtTheBottom = True
    )
    atlasLabels = ["#font[72]{ATLAS} Simulation Internal","t#bar{t} sample, 15.5M events","Match range: #Delta #phi<0.3 and #Delta#eta<0.6"]
    lh.addLabel(atlasLabels)
    legList = []
    legList.append([gr1, "Jet", "p"])
    legList.append([gr2, "Electron", "p"])
    legList.append([gr3, "Tau", "p"])
    lh.addLegend([l[1] for l in legList])
    
    #decorations = ["#\epsilon = #frac{N ( L1 |#eta| #leqslant cut && 2.3 < Offline |#eta| < 2.5 ) }{N(\ 2.3 < Offline\ |#eta|<2.5 \ )}", "another text", "another text"]
    #lh.addText(decorations)
    if not lh.placeAllTextToPad():
        lh.clearStorage()
        lh.resetAttributesToNone()
        for p in pads:
            p.IsA().Destructor(p)
        return
    # Drawing the pad frame
    ph.drawFrame(lh.getFrame().Clone(), "|#kern[0.1]{#eta}_{L1}| cut", "#varepsilon = #frac{N( |#kern[0.1]{#eta_{L1}}|#leqcut and 2.3<|#kern[0.1]{#eta_{Offline}}|<2.5 )}{N( 2.3<|#kern[0.1]{#eta_{Offline}}|<2.5 )}")
    #ph.drawFrame(lh.getFrame().Clone(), "L1 |#eta| cut", "L1 Jet Efficiency")
    # Drawing text objects to the pad frame
    ph.drawTextBox(atlasLabels, lh.labelCoordinates()["TLatexPositions"])
    ph.drawLegend(legList, {'y1': 0.40, 'x2': 0.95, 'x1': 0.70, 'nColumns': 1, 'y2': 0.20})
    #ph.drawTextBox(decorations, lh.textCoordinates(0)["TLatexPositions"])
    
    gr1.SetMarkerColor(1)
    gr1.SetMarkerStyle(20)
    gr1.SetLineColor(1)
    gr1.SetLineWidth(2)
    gr1.SetMarkerSize(1)
    gr1.Draw("P same")
    
    gr2.SetMarkerColor(4)
    gr2.SetMarkerStyle(21)
    gr2.SetLineColor(4)
    gr2.SetLineWidth(2)
    gr2.SetMarkerSize(1)
    gr2.Draw("P same")
    
    gr3.SetMarkerColor(8)
    gr3.SetMarkerStyle(22)
    gr3.SetLineColor(8)
    gr3.SetLineWidth(2)
    gr3.SetMarkerSize(1)
    gr3.Draw("P same")
    
    #c1.Range( 0., -10., 1., 10. )
    #line = TLine(.,0,2.5,1.2);
    #line.SetLineColor(kYellow)
    #line.SetLineWidth(2)
    #line.Draw()
    
    middleLine = TLine(2.5,0,2.5,1.3)
    middleLine.SetLineColor(2)
    middleLine.SetLineWidth(2)
    middleLine.SetLineStyle(2)
    middleLine.Draw("same")
    
    pads[0].RedrawAxis()
    c1=pads[len(pads) - 1]
    c1.SaveAs(fileOut)
    
    raw_input("press enter")
    # Terminate the session
    lh.clearStorage()
    lh.resetAttributesToNone()
    for p in pads:
        p.IsA().Destructor(p)
    return


if __name__ == '__main__': 
    exampleSimplePlot(sys.argv[1])
