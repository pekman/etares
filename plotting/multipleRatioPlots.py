from ROOT import *
from sys import *

from TH1_helpers import TH1_helper
from layout_helpers import layout_helper
from plotting_helpers import ratioPlot

def graphDivide(gr1,gr2):
    grRatio = gr1.Clone()
    
    nominal_iterator=0
    physics_iterator=0
    while nominal_iterator < gr1.GetN():
        x1=Double(0)
        y1=Double(0)
        x2=Double(0)
        y2=Double(0)
        gr1.GetPoint(nominal_iterator,x1,y1)
        gr2.GetPoint(physics_iterator,x2,y2)
        #print(x1,x2)
        if x1<x2:
            #print("------ Your TGraphs are out of synch. Correcting... ------")
            #nominal_iterator+=1
            nominal_iterator+=1
            continue
        elif x1>x2:
            #print("------ Your TGraphs are out of synch. Correcting... ------")
            #nominal_iterator+=1
            physics_iterator+=1
            continue
        
            
        try:
            grRatio.SetPoint(nominal_iterator,x1,y2/y1)
        except ZeroDivisionError:
            pass
        physics_iterator+=1
        nominal_iterator+=1
    return grRatio

def getRatioList(datasetList, plots):
    ratioList=[]
    for i in range(1,len(datasetList)):
        print("dataset:")
        print(datasetList[i])
        ratioList.append(graphDivide(plots[0],plots[i]))
    return ratioList


def multipleRatioPlots(plots,parameters,plotParameters,c1,jobNameList,currentSlice):

    ph = ratioPlot()
    ph.setStyle(24)
    lh = layout_helper()
    
    # Prepare canvas and pads
    pads = ph.canvasAndPads(False,plotParameters["logx"])
    lh.resetAttributes(pad = pads[0], plots = plots
                       , xMin = plotParameters["xMin"], xMax = plotParameters["xMax"]
                       , yMin = plotParameters["yMin"], yMax = plotParameters["yMax"]
                       , legendNCol = 1
                       , letAtlasLabelFloat = False, yMaxOffset = 0.05, yMinOffset = 0.05
                       #, textPlotsDist = 0.04999
                       , considerUncertainties = True
                       , topTextPosY = ph.topTextPosY, bottomTextPosY = ph.bottomTextPosY
                       , verbose = True
                       #, entryHeight = 0.065
                       , entryHeight = 0.090
                       , putTextAtTheTop = True, putTextAtTheBottom = True
    )
    
    #ATLAS labels
    atlasLabels = plotParameters["atlasLabels"][:]
    #atlasLabels.append(plotParameters["sliceTitle"]+" = "+str(currentSlice)[1:-1].split(",")[0]+" -"+str(currentSlice)[1:-1].split(",")[1]+" "+plotParameters["sliceUnit"])
    atlasLabels.append(str(round(float(str(currentSlice)[1:-1].split(",")[0]),1)) +"<"+ plotParameters["sliceTitle"]+"<"+str(round(float(str(currentSlice)[1:-1].split(",")[1]),1))+" "+plotParameters["sliceUnit"])
    lh.addLabel(atlasLabels)
    
    #Legend
    legList = []
    i=0
    #"user.pekman.Klong.426343.e6668.s3170.r10571.r1064.v1_tree.root"
    for p in plots:
        #legList.append([p, plotParameters["legendVariable"]+" = "+str(float(plotDict["choosenSlices"][i][0]))+plotParameters["legendUnit"], "pl"])
        #print(jobNameList[i].split(".")[2].split("_")[0]+"_"+jobNameList[i].split(".")[2].split("_")[2])
        particleName = jobNameList[i].split(".")[2].split("_")[0]
        if(particleName == "piplus"):
            legList.append([p, ""+jobNameList[i].split(".")[2].split("_")[2], "pl"])
        elif(particleName == "piminus"):
            legList.append([p, ""+jobNameList[i].split(".")[2].split("_")[2], "pl"])
        elif(particleName == "Klong"):
            particleName = jobNameList[i].split(".")[5]
            legList.append([p, particleName, "pl"])
        i += 1
    lh.addLegend([l[1] for l in legList])
    lh.addLegend([l[1] for l in legList])
    #Decorations
    #decorations = ["text", "text", "text", "another text", "another text"]
    #lh.addText(decorations)
    
    # Calculate the optimal layout
    if not lh.placeAllTextToPad():
        return
    
    # Drawing the top pad frame
    ph.drawTopFrame(lh.getFrame().Clone(),plotParameters["yTitle"])
    
    # Drawing text objects to the top pad frame
    ph.drawTextBox(atlasLabels, lh.labelCoordinates()["TLatexPositions"])
    ph.drawLegend(legList, plotParameters["legendCoords"])
    
    #Plotting plots
    colors = [kBlack,
              kTeal-1,
              kOrange-3,#
              kRed+1,#
              kGreen+1,#
              kViolet-1,
              kOrange-3,
              #kMagenta,
              kBlue+1]
    styles = [24,25,26,27,28]

    iColors = 1
    iStyles =1
     
    for p in plots[1:]:
        if iStyles==len(styles):
            iStyles=0
        if iColors==len(colors):
            iColors=0
        p.Draw("P same")
        p.SetMarkerStyle(styles[iStyles])
        p.SetMarkerColor(colors[iColors])
        p.SetLineColor(colors[iColors])
        p.SetMarkerSize(1)
          
        iStyles+=1
        iColors+=1
    
    plots[0].Draw("P same")
    plots[0].SetMarkerStyle(24)
    plots[0].SetMarkerColor(kBlack)
    plots[0].SetLineColor(kBlack)
    plots[0].SetMarkerSize(1)
    print("Error:")
    print(plots[0])

    pads[0].RedrawAxis()
    
    # ----------------------
    # Bottom pad
    # ----------------------
    # Prepare the ratio
    
    ratioList = getRatioList(parameters["datasetList"], plots)
    
    # Setup layout_helper
    lh.resetAttributes(pad = pads[1], plots = ratioList
                       , yMin=plotParameters["ratioYMin"], yMax=plotParameters["ratioYMax"]
                       , xMin = plotParameters["xMin"], xMax = plotParameters["xMax"]
                       , verbose = True
                       , considerUncertainties = False
                       , yMinOffset = 0.06, yMaxOffset = 0.06
    )
    
    # Calculate the optimal layout
    if not lh.placeAllTextToPad():
        lh.clearStorage()
        lh.resetAttributesToNone()
        for p in pads:
            p.IsA().Destructor(p)
        return
    
    # Draw objects
    ph.drawBottomFrame(lh.getFrame().Clone(),plotParameters["xTitle"], "Ratio to Nominal")
    
    iColors = 1 #Important 1 so that it does not associate black with the non-existent nominal ratio plot
    iStyles = 1
    
    for r in ratioList: 
        if iStyles==len(styles):
            iStyles=0
        if iColors==len(colors):
            iColors=0
        r.Draw("PL same")
        r.SetMarkerStyle(styles[iStyles])
        r.SetMarkerColor(colors[iColors])
        r.SetLineColor(colors[iColors])
        r.SetLineWidth(1)
        r.SetMarkerSize(0.5)
        
        iStyles+=1
        iColors+=1
    
    lh.pad.RedrawAxis()
    
    #if len(plotParameters["lineList"])>0:
#     topLine = plotParameters["lineList"][2]
#     topLine.SetLineColor(kRed)
#     topLine.SetLineWidth(1)
#     topLine.Draw("same")
    
    middleLine = plotParameters["lineList"][0]
    middleLine.SetLineColor(kBlack)
    middleLine.SetLineWidth(2)
    middleLine.SetLineStyle(2)
    middleLine.Draw("same")
    
#     bottomLine = plotParameters["lineList"][0]
#     bottomLine.SetLineColor(kRed)
#     bottomLine.SetLineWidth(1)
#     bottomLine.Draw("same")
    
    #Save
    c1=pads[len(pads) - 1]
    
    #c1.SaveAs(parameters["datasetName"]+":"+plotParameters["sliceTitle"]+"="+str(currentSlice)+"-"+plotParameters["fileName"]) 
    c1.SaveAs(plotParameters["fileName"][0]+str(currentSlice[0])+","+str(currentSlice[1])+plotParameters["fileName"][1])
    
    
    # Terminate the session
    lh.clearStorage()
    lh.resetAttributesToNone()
    for p in pads:
        p.IsA().Destructor(p)
    return
