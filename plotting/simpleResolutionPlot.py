from TH1_helpers import TH1_helper
from layout_helpers import layout_helper
from plotting_helpers import simplePlot

#plots = list of graphs/histograms
#plotDetails = corresponding value from plotDict 
#canvas = canvas into which you want the graph
#labelString = string you want in output files and in the plot

def simpleResolutionPlot(plots, plotDetails, canvas, labelString):
   
    ph = simplePlot()
    ph.setStyle(24)
    lh = layout_helper()
    # Prepare canvas and pads
    pads = ph.canvasAndPads(setLogy = False, setLogx = plotDetails["plotParameters"]["doLogx"])
    # Calculate text objects coordinates and draw all objects.
    lh.resetAttributes(pad = pads[0], plots = plots
                       , xMin = plotDetails["plotParameters"]["xMin"], xMax = plotDetails["plotParameters"]["xMax"]
                       , yMin = plotDetails["plotParameters"]["yMin"], yMax = plotDetails["plotParameters"]["yMax"]
                       , legendNCol = 4
                       , letAtlasLabelFloat = False, yMaxOffset = 0.05, yMinOffset = 0.05
                       , textPlotsDist = 0.04999
                       , considerUncertainties = True
                       , topTextPosY = ph.topTextPosY, bottomTextPosY = ph.bottomTextPosY
                       , verbose = True, entryHeight = 0.065
                       , putTextAtTheTop = True, putTextAtTheBottom = True
        )
    atlasLabels = [labelString," "]
    lh.addLabel(atlasLabels)
    legList = []
    i=0
    for p in plots:
        legList.append([p, plotDetails["plotParameters"]["legendVariable"]+" = ["+str(plotDetails["slices"][i][0])+", "+str(plotDetails["slices"][i][1])+"]"+plotDetails["plotParameters"]["legendUnit"], "pl"])
        i += 1
    lh.addLegend([l[1] for l in legList])
    if not lh.placeAllTextToPad():
        lh.clearStorage()
        lh.resetAttributesToNone()
        for p in pads:
            p.IsA().Destructor(p)
        return
    # Drawing the pad frame
    ph.drawFrame(lh.getFrame().Clone(),plotDetails["plotParameters"]["xTitle"],plotDetails["plotParameters"]["yTitle"])
    # Drawing text objects to the pad frame
    ph.drawTextBox(atlasLabels, lh.labelCoordinates()["TLatexPositions"])
    
    #these might have to change based on the plot you want
    coords = {'y1': 0.6, 'x2': 0.9, 'x1': 0.2, 'nColumns': 2, 'y2': 0.9}
    
    ph.drawLegend(legList, coords)
    colors = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 28, 32, 38, 41, 42, 45, 46, 49]
    styles = [24, 25, 26, 27, 28, 30, 32, 33, 22, 23, 20, 21]
    iColors = 0
    iStyles =0
    for p in plots:
        if iStyles==len(styles):
            iStyles=0
        if iColors==len(colors):
            iColors=0
        p.Draw("PL")
        p.SetMarkerStyle(styles[iStyles])
        p.SetMarkerColor(colors[iColors])
        p.SetLineColor(colors[iColors])
        p.SetMarkerSize(0.8)
        iStyles+=1
        iColors+=1
        p.SetName(labelString)
        p.SaveAs(labelString+"_graphs.root","RECREATE")

    pads[0].RedrawAxis()
    canvas=pads[len(pads) - 1]
    canvas.Print(plotDetails["plotParameters"]["fileName"],"pdf");
    # Terminate the session
    lh.clearStorage()
    lh.resetAttributesToNone()
    for p in pads:
        p.IsA().Destructor(p)
    return

