from ROOT import * 
from array import array
from JES_BalanceFitter import JES_BalanceFitter

#this helper takes a TH3 and slices it (according to arguments), fits each slice with a gaussian and returns a TGraphErrors of responses or resolutions
    
#graphType = type of graph desired ("res, "relRes", "resp"); inTH3 = input TH3; slicingAxis = axis to bin over; projectionAxis = axis to integrate out; responseAxis = axis with response; sliceBins = bins into which slicingAxis is sliced; rebinningSize = # of bins to combine; outFile = place to save outputs; projectionRebinWidth = how many bins to combine to make each new bin on the projection axis; nSigmaForFit = the fitting range in terms of sample variance

def TH3toTGraphs(graphType, inTH3, slicingAxis, projectionAxis, responseAxis, sliceBins, projectionRebinWidth, outFile, nSigmaForFit, fitOptString):
     
    #quantities to fill TGraphErrors with
    xList, yList, xErrorList, yErrorList, sigmaList, sigmaErrorList, sigmaOverYList = array( 'f' ), array( 'f' ), array( 'f' ), array( 'f' ), array( 'f' ), array( 'f' ), array( 'f' )
    graphsToReturn = []   
    h3D = inTH3.Clone()

    #set JES_BalanceFitter options
    JESBfitter = JES_BalanceFitter(nSigmaForFit)
    JESBfitter.SetGaus()
    JESBfitter.SetFitOpt(fitOptString)
    
    #if outFile exists, we want to save TH3 and the derived TH1s to this file
    if outFile:
        saveOutputToFile = True
        outFile = TFile.Open(outFile, "RECREATE")
        
    xnBins = h3D.GetXaxis().GetNbins()
    ynBins = h3D.GetYaxis().GetNbins()

    for bin in sliceBins:
        print "Doing bin", bin

        #Reset the arrays
        del xList[:]
        del yList[:]        
        del xErrorList[:]
        del yErrorList[:]
        del sigmaErrorList[:]
        del sigmaList[:]
        del sigmaOverYList[:]
             
        #Get the bin which corresponds to the desired slice axis range
        if slicingAxis == "y":
            h3D.GetYaxis().SetRangeUser(bin[0], bin[1]) 
            
        elif slicingAxis == "z":
            h3D.GetZaxis().SetRangeUser(bin[0], bin[1]) 

        if saveOutputToFile:
            outFile.cd()  
            h3D.Write()
                          
        #Project the 3D histogram with the set y-axis range
        h2D=h3D.Project3D(responseAxis + projectionAxis)
         
        #save projected TH3->TH2 to file 
        if saveOutputToFile:
            h2D.Write()
 
        #rebin according to the desired rebinningFactor
        h2D.RebinX(projectionRebinWidth)
                
        currentRebinnedBin = 1; 
        for currentRebinnedBin in range(1, h2D.GetNbinsX()+1):
            
            #name of projection
            projName = "slice"+str(bin[0])+"to"+str(bin[1])+"_projectionBin"+str(h2D.GetXaxis().GetBinLowEdge(currentRebinnedBin))+"to"+str(h2D.GetXaxis().GetBinUpEdge(currentRebinnedBin))
 
            #take projection
            h1D=h2D.ProjectionY(projName, currentRebinnedBin, currentRebinnedBin)
            
            #skip empty bins
            if h1D.GetEntries() == 0:
                print "empty 1D hist, skipping!"
                continue
            
            #fitting limits
            fitMax = h1D.GetMean() + nSigmaForFit * h1D.GetRMS()
            fitMin = h1D.GetMean() - nSigmaForFit * h1D.GetRMS()
            
            #obtain fit using JES_BalanceFitter and associate it to the TH1           
            JESBfitter.Fit(h1D, fitMin, fitMax)
            fit = JESBfitter.GetFit()
            histFit = JESBfitter.GetHisto()
            histFit.GetListOfFunctions().Add(fit)

            if saveOutputToFile:
                gStyle.SetOptStat(0);
                gROOT.ForceStyle();
                
                #histFit.SetTitle("p_{T} Range ("+str(round(h2D.GetXaxis().GetBinLowEdge(currentRebinnedBin),2))+", "+str(round(h2D.GetXaxis().GetBinUpEdge(currentRebinnedBin),2))+") GeV. Fit Range: ("+str(round(fitMin,2))+", "+str(round(fitMax,2))+"). Entries: "+str(h1D.GetEntries()))
                histFit.SetTitle("|#eta| Range ("+str(round(h2D.GetXaxis().GetBinLowEdge(currentRebinnedBin),2))+", "+str(round(h2D.GetXaxis().GetBinUpEdge(currentRebinnedBin),2))+"). Fit Range: ("+str(round(fitMin,2))+", "+str(round(fitMax,2))+"). Entries: "+str(h1D.GetEntries()))
                gStyle.SetTitleFontSize(0.8)
                histFit.GetXaxis().SetTitle("p_{T} Response")
                binWidth = h2D.GetXaxis().GetBinUpEdge(currentRebinnedBin) - h2D.GetXaxis().GetBinLowEdge(currentRebinnedBin)
                #histFit.GetYaxis().SetTitle("Counts / "+str(round(binWidth,2))+" GeV^{-1}")
                histFit.GetYaxis().SetTitle("Counts / "+str(round(binWidth,2)))
                
                histFit.Write()
                 
            #append return graph values
            x = float(h2D.GetXaxis().GetBinCenter(currentRebinnedBin))
            y = float(fit.GetParameter(1))
            xError = float((h2D.GetXaxis().GetBinWidth(currentRebinnedBin)/2.0)) #half bin width
            yError = float(fit.GetParError(1))
            sigma = float(fit.GetParameter(2))
            sigmaError = float(fit.GetParError(2))
            
            try: 
                sigmaOverY = float(fit.GetParameter(2) / float(fit.GetParameter(1)))
            except: 
                sigmaOverY = 0
            
            yList.append(y)
            xList.append(x)
            yErrorList.append(yError)
            xErrorList.append(xError)
            sigmaList.append(sigma)
            sigmaErrorList.append(sigmaError)           
            sigmaOverYList.append(sigmaOverY)
            
        #Create a TGraph from the arrays
        if graphType == "resolution":
            gr = TGraphErrors(len(xList), xList, sigmaList, xErrorList, SigmaErrorList)
        elif graphType == "response":  
            gr = TGraphErrors(len(xList), xList, yList, xErrorList, yErrorList)
        #elif graphType == "relativeResponse": 
            #gr = TGraphErrors( len(xList), xList, sigmaOverYList, xErrorList, SigmaErrorList) #FIXME add error to relative resolution. 
        else: 
            raise Exception("TH3toTGraphs() ERROR: no known graphType provided")
            

        #set unique name for graph
        gr.SetName(graphType+"_slice"+str(bin[0])+"to"+str(bin[1]))

        #append this graph to return list
        graphsToReturn.append(gr)
        
        #also write to output file if enabled
        if saveOutputToFile:
            gr.Write()
    
    outFile.Close()       
    
    return graphsToReturn
